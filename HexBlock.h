//
// FILE: HEXBLOCK.H
//

#ifndef HexBlock_h
#define HexBlock_h

#include <istream>
#include <ostream>

// The block size to read in bytes.
// It is suggested to only use multiples of 8; other values may break formatting.
const int BLOCK_SIZE = 16;

class HexBlock
{
private:
    unsigned char fBuffer[BLOCK_SIZE];
    unsigned long fSize;
public:
    // default constructor: set buffer elements to zero and size to 16
    HexBlock();

    // read up to 16 bytes (i.e. raw characters) into buffer
    // records the number of characters read in size (<= 16)
    std::istream& read( std::istream& aIStream );

    // getter to return characters in buffer
    unsigned long size() const;

    // formatted output operator: produces a hex dump line
    friend std::ostream& operator<<( std::ostream& aOStream, const HexBlock& aObject );
};

#endif /* HexBlock_h */
