//
// FILE: HEXBLOCK.CPP
//

#include "HexBlock.h"
#include <iostream>
#include <iomanip>

using namespace std;

HexBlock::HexBlock() {
    fSize = BLOCK_SIZE;
    for (int i = 0; i < BLOCK_SIZE; i++) {
        fBuffer[i] = 0;
    }
}

std::istream& HexBlock::read( std::istream& aIStream ) {
    // Grab the next block from file
    aIStream.read((char *)fBuffer, BLOCK_SIZE);
    fSize = aIStream.gcount();

    return aIStream;
}

unsigned long HexBlock::size() const {
    return fSize;
}

std::ostream& operator<<( std::ostream& aOStream, const HexBlock& aObject ) {
    // Used to calculate whitespace fill for allignment of 'actual ASCII' section
    // The line printed (by HexBlock) is always this width.
    // This takes into account each hex value, with the spaces in between, and also the chunk dividers.
    int lSpaces = (BLOCK_SIZE * 3) + (BLOCK_SIZE / 4);

    // Hexadecimal representation section
    for (int i = 0; i < aObject.fSize; i++ ) {
        // Do we need a chunk divider?
        if (i % 8 == 0 && i > 0) {
            cout << "| ";
            lSpaces -= 2;
        }

        cout << hex << uppercase << setw(2) << (int)aObject.fBuffer[i] << " ";
        lSpaces -= 3;
    }

    // Apply the correct allignment spacing we calculated earlier
    cout << setfill(' ') << setw(lSpaces);

    for (int i = 0; i < aObject.fSize; i++ ) {
        if (aObject.fBuffer[i] <= ' ' || aObject.fBuffer[i] >= 127) {
            // Replace any non-printable character
            cout << ".";
        } else {
            cout << aObject.fBuffer[i];
        }
    }

    return aOStream;
}
