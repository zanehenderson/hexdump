//
// FILE: HEXDUMP.CPP
//

#include "HexDump.h"
#include <fstream>
#include <iostream>
#include <iomanip>

using namespace std;

bool HexDump::open(const std::string &aInputFileName) {
    fInput.open(aInputFileName, ifstream::binary);
    return fInput.good();
}

void HexDump::close() {
    fInput.close();
    return;
}

bool HexDump::operator()( const std::string& aInputFileName ) {
    bool lFileOpened = open(aInputFileName);

    if (lFileOpened) {
        processInput();
    }

    close();
    return lFileOpened;
}

void HexDump::processInput() {
    // Hex offset of the file
    unsigned int lOffset = 0;

    do {
        fData.read(fInput);
        if (fData.size() > 0) {
            cout << hex << setfill('0') << setw(8) << lOffset << ":  " << fData << endl;
        }
        lOffset += BLOCK_SIZE;
    } while (fData.size() == BLOCK_SIZE);

    return;
}
