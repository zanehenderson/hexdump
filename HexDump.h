//
// FILE: HEXDUMP.H
//

#ifndef HexDump_h
#define HexDump_h

#include <stdio.h>
#include <fstream>
#include <string>
#include "hexblock.h"

class HexDump
{
private:
    HexBlock fData;
    std::ifstream fInput;
public:
    // open input file
    bool open( const std::string& aInputFileName );

    // close input file
    void close();

    // Generates a hex dump and outputs to standard output
    void processInput();

    // Make objects of type HexDump callable. We use objects of type HexDump
    // as functions that take a file name as parameter and return a bool to
    // indicate success or failure of producing a hex dump to standard
    // output. The result is false only if we could not open input.
    bool operator()( const std::string& aInputFileName );
};

#endif /* HexDump_h */
